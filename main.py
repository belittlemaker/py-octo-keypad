from config import ConfigLogging
from config import ConfigOctoPrint
from config import ConfigPins
from devices.buttons.FactoryButtons import FactoryButtons
from devices.buttons.IButton import IButton
from devices.Keyboard import Keyboard
from devices.leds.FactoryLeds import FactoryLeds
from devices.leds.Leds import Leds
from libs.GPioWrapper import GPioWrapper
from libs.SignalHandler import SignalHandler
import logging
from pyoctoprintapifacade import OctoPrintApi
from typing import List
import signal

try:
    signal_handler = SignalHandler()
    signal.signal(signal.SIGTERM, signal_handler.handler)

    GPioWrapper.boot()

    logging.basicConfig(filename=ConfigLogging.file_name,
                        filemode=ConfigLogging.mode,
                        level=ConfigLogging.level,
                        format=ConfigLogging.output_format,
                        datefmt=ConfigLogging.date_format)
    logging.info('Init')

    printer_api: OctoPrintApi = OctoPrintApi(ConfigOctoPrint.api_key, ConfigOctoPrint.domain)

    buttons: List[IButton] = [
        FactoryButtons.get_cancel_button(ConfigPins.pin_cancel, printer_api),
        FactoryButtons.get_home_button(ConfigPins.pin_home, printer_api),
        FactoryButtons.get_hot_bed_button(ConfigPins.pin_hot_bed, printer_api),
        FactoryButtons.get_hot_tool_button(ConfigPins.pin_hot_tool, printer_api),
        FactoryButtons.get_level_button(ConfigPins.pin_level, printer_api),
        FactoryButtons.get_pause_button(ConfigPins.pin_pause, printer_api)
    ]

    led_green = FactoryLeds.get_led(ConfigPins.pin_green)
    led_red = FactoryLeds.get_led(ConfigPins.pin_red)

    leds = Leds(led_green, led_red)

    keyboard = Keyboard(buttons, leds)

    signal_handler.set_keyboard(keyboard)

    keyboard.start()

except Exception as exception:
    logging.error(str(exception))
finally:
    GPioWrapper.cleanup()
    logging.info('End')
