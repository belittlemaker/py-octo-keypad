from pyoctoprintapifacade import OctoPrintApi
from typing import List


class Environment:

    __position: List[int] = []

    def __init__(self, printer_api: OctoPrintApi):
        self.__printer_api: OctoPrintApi = printer_api

        self.__current_state: str = ''
        self.__last_state: str = ''
        self.__home: List[int] = [0, 0, 0]

    def has_state_changed(self) -> bool:
        return self.__current_state != self.__last_state

    def get_job_state(self) -> str:
        job_details: dict = self.__printer_api.get_job_facade().get_current_job()

        state = job_details['state'] if 'state' in job_details else ''

        self.__update_state(state)

        return self.__current_state

    def homing(self) -> None:
        Environment.__position = self.__home

    def is_home(self) -> bool:
        return Environment.__position == self.__home

    def update_position(self, position: List[int]) -> None:
        Environment.__position = position

    def __update_state(self, state: str) -> None:
        self.__last_state = self.__current_state
        self.__current_state = state
