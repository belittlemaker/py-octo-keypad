from devices.Keyboard import Keyboard
from libs.GPioWrapper import GPioWrapper
import logging
import sys


class SignalHandler:

    __keyboard: Keyboard

    def handler(self, sig, frame) -> None:
        if self.__keyboard:
            self.__keyboard.close()

        GPioWrapper.cleanup()

        logging.info('Keyboard closing Signal')

        sys.exit(0)

    def set_keyboard(self, keyboard: Keyboard) -> None:
        self.__keyboard = keyboard
