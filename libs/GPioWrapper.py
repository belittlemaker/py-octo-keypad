import RPi.GPIO as GPIO


class GPioWrapper:

    @staticmethod
    def  boot(mode=GPIO.BCM) -> None:
        GPIO.setmode(mode)

    @staticmethod
    def cleanup() -> None:
        GPIO.cleanup()

    @staticmethod
    def get_input_state(channel: int) -> any:
        return GPIO.input(channel)

    @staticmethod
    def set_output_state_high(channel: int) -> None:
        GPIO.output(channel, GPIO.HIGH)

    @staticmethod
    def set_output_state_low(channel: int) -> None:
        GPIO.output(channel, GPIO.LOW)

    @staticmethod
    def setup_channel_input(channel: int) -> None:
        GPIO.setup(channel, GPIO.IN, pull_up_down=GPIO.PUD_UP)

    @staticmethod
    def setup_channel_output(channel: int, initial: int=GPIO.LOW) -> None:
        GPIO.setup(channel, GPIO.OUT, initial=initial)
