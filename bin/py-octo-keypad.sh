#!/bin/bash
# remember, this file script must have execution permissions (chmod +x)

# own project folder path
PROJECT_PATH=/home/pi/py-octo-keypad

# Go to the specified directory
cd "$PROJECT_PATH" || { echo "Directory does not exist"; exit 1; }

# execute keyboard
python3 main.py