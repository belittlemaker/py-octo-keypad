import logging
from datetime import datetime

# DOC: https://docs.python.org/3/library/logging.html
# own logging configuration

path = './log/'
base_name = 'log_'
base_date = '%Y%m%d'
extension = '.log'
file_name = path + base_name + datetime.now().strftime(base_date) + extension
level = logging.INFO
output_format = '%(asctime)s - %(levelname)s - %(message)s'
date_format = '%Y-%m-%d %H:%M:%S'
mode = 'a'
