# py-octo-keypad
Python OctoPrint controller for keypad connected to the RaspberryPi GPIOs

Keypad design
--------------
* https://www.thingiverse.com/thing:6674580

Steps before
------------
### Install OctoPrint API ###
```
python3 -m pip install git+https://gitlab.com/belittlemaker/py-octoprint-api-facade.git@master
```
### Add to Crontab ###
```
@reboot sh <own project base path>/py-octo-keypad/bin/py-octo-keypad.sh
```
### Review files & choose your own configuration ###
- Buttons pins:
  - config/ConfigPins.py
- OctoPrint connection profile:
  - config/ConfigOctoPrint.py
- Python Logging library configuration:
  - config/ConfigLogging.py
- Location project path:
  - bin/py-octo-keypad.sh
- Bed leveling, limit positions to check:
  - devices/buttons/LevelButton.py
- Bed heating, temperatures to heat:
  - devices/buttons/HotBedButton.py
- Tool heating, temperatures to heat:
  - devices/buttons/HotToolButton.py

### Environment ###
Tested and functional on:
- Python 3.8
- OctoPrint 1.10.2
