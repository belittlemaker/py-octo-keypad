from config import ConfigGeneral
from devices.buttons.IButton import IButton
from devices.leds.Leds import Leds
from time import sleep
from typing import List
import logging


class Keyboard:

    def __init__(self, buttons: List[IButton], leds: Leds):
        self.__active: bool = False
        self.__buttons: List[IButton] = buttons
        self.__leds: Leds = leds

    def close(self) -> None:
        self.__active = False
        self.__leds.turn_off()

    def start(self) -> None:
        self.__active = True
        self.__leds.turn_on()
        self.__observe()

    def __observe(self) -> None:
        while self.__active:
            for button in self.__buttons:
                try:
                    self.__observe_button(button)
                except Exception as exception:
                    logging.error(str(exception))

            sleep(ConfigGeneral.loop_pause)

    def __observe_button(self, button: IButton) -> None:
        if button.is_pushed():
            if button.is_allowed_action():
                button.action()
                self.__leds.success()
            else:
                button.reset()
                self.__leds.fail()
