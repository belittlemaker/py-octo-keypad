from devices.leds.Led import Led


class FactoryLeds:

    @staticmethod
    def get_led(pin: int) -> Led:
        return Led(pin)
