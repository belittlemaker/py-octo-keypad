from devices.leds.Led import Led


class Leds:

    def __init__(self, success_led: Led, fail_led: Led):
        self.__fail_led: Led = fail_led
        self.__success_led: Led = success_led

    def fail(self) -> None:
        self.__fail_led.blink()

    def success(self) -> None:
        self.__success_led.blink()

    def turn_off(self) -> None:
        self.__success_led.blink()
        self.__fail_led.blink()

    def turn_on(self) -> None:
        self.__fail_led.blink()
        self.__success_led.blink()
