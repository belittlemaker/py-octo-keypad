from config import ConfigGeneral
from libs.GPioWrapper import GPioWrapper
from time import sleep


class Led:

    def __init__(self, pin: int):
        self._pin: int = pin
        GPioWrapper.setup_channel_output(self._pin)

    def blink(self, time: float = ConfigGeneral.blink_pause):
        self.turn_on()
        sleep(time)
        self.turn_off()

    def turn_off(self):
        GPioWrapper.set_output_state_low(self._pin)

    def turn_on(self):
        GPioWrapper.set_output_state_high(self._pin)
