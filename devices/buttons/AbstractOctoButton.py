import logging
from abc import ABC, abstractmethod
from config import ConfigOctoPrint
from devices.buttons.AbstractButton import AbstractButton
from printer.Environment import Environment
from pyoctoprintapifacade import OctoPrintApi


class AbstractOctoButton(AbstractButton, ABC):

    def __init__(self, pin: int, printer_api: OctoPrintApi):
        super().__init__(pin=pin)

        self._environment = Environment(printer_api)
        self._printer_api = printer_api

    def action(self) -> None:
        self._reset_environment_state_changed()
        self._action_local()

    def is_allowed_action(self) -> bool:
        try:
            is_operational = self._printer_api.get_connection_facade().is_operational()

            if not is_operational and ConfigOctoPrint.auto_connection:
                self._printer_api.get_connection_facade().connect()

        except Exception as exception:
            logging.error(str(exception))
            is_operational = False

        return is_operational

    @abstractmethod
    def _action_local(self) -> None:
        pass

    def _reset_environment_state_changed(self) -> None:
        if self._environment.has_state_changed():
            self.reset()
