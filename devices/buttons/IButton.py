from abc import ABC, abstractmethod


class IButton(ABC):

    @abstractmethod
    def action(self) -> None:
        pass

    @abstractmethod
    def get_label(self) -> str:
        pass

    @abstractmethod
    def is_allowed_action(self) -> bool:
        pass

    @abstractmethod
    def is_pushed(self) -> bool:
        pass

    @abstractmethod
    def reset(self) -> None:
        pass
