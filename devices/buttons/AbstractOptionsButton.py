from abc import ABC, abstractmethod
from devices.buttons.AbstractButton import AbstractButton
from itertools import cycle


class AbstractOptionsButton(AbstractButton, ABC):

    __options: cycle = None

    def reset(self) -> None:
        self.__options: cycle = cycle(self._get_options())

    def _get_option(self) -> any:
        return next(self.__options)

    @abstractmethod
    def _get_options(self) -> list:
        pass
