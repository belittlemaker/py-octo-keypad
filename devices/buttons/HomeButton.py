from devices.buttons.AbstractOutJobOctoButton import AbstractOutJobOctoButton


class HomeButton(AbstractOutJobOctoButton):

    def _action_local(self) -> None:
        self._printer_api.get_printer_facade().go_home()
        self._environment.homing()
