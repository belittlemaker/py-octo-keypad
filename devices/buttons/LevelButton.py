from devices.buttons.AbstractOutJobOctoButton import AbstractOutJobOctoButton
from devices.buttons.AbstractOptionsButton import AbstractOptionsButton
from typing import List

class LevelButton(AbstractOutJobOctoButton, AbstractOptionsButton):

    __speed: int = 1500

    def _action_local(self) -> None:
        self.__check_position()

        position = self._get_option()

        if not position:
            self._printer_api.get_printer_facade().go_home()
        else:
            self.__move(position)

    def _get_options(self) -> list:
        # x, y, z (bed 220x220)
        # explore own bed limits
        return [
            [],               # Home
            [  40,   60, 0],  # Home to bottom - left
            [ 140,    0, 0],  # to bottom - right
            [   0,  160, 0],  # to up - right
            [-140,    0, 0],  # to up - left
            [   0, -160, 0],  # to bottom - left
        ]

    def __move(self, position: List[int]) -> None:
        move_x, move_y, move_z = position

        self._printer_api.get_printer_facade().jog(move_x,
                                                   move_y,
                                                   move_z,
                                                   self.__speed)
        self._environment.update_position(position)

    def __check_position(self) -> None:
        if self._environment.is_home():
            self.reset()
            self._get_option()
