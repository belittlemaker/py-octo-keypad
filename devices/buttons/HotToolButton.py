from devices.buttons.AbstractOutJobOctoButton import AbstractOutJobOctoButton
from devices.buttons.AbstractOptionsButton import AbstractOptionsButton


class HotToolButton(AbstractOutJobOctoButton, AbstractOptionsButton):

    def _action_local(self) -> None:
        temperature: int = self._get_option()

        command: str = 'M104 S{}'.format(temperature)

        self._printer_api.get_printer_facade().printer_command([command])

    def _get_options(self) -> list:
        # explore own tool temperatures
        return [
            200, # PLA > print
            210, # PLA > change filament
            0,   # cold
        ]
