from abc import ABC
from devices.buttons.IButton import IButton
from libs.GPioWrapper import GPioWrapper
import logging


class AbstractButton(IButton, ABC):

    def __init__(self, pin: int):
        self._pin: int = pin
        GPioWrapper.setup_channel_input(self._pin)
        self.reset()

    def get_label(self) -> str:
        return self.__class__.__name__

    def is_allowed_action(self) -> bool:
        return True

    def is_pushed(self) -> bool:
        pushed: bool = not GPioWrapper.get_input_state(self._pin)

        if pushed:
            logging.info(self.get_label())

        return pushed

    def reset(self) -> None:
        pass
