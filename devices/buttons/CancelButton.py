from devices.buttons.AbstractInJobOctoButton import AbstractInJobOctoButton


class CancelButton(AbstractInJobOctoButton):

    def _action_local(self) -> None:
        self._printer_api.get_job_facade().cancel()
