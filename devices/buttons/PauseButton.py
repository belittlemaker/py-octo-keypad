from devices.buttons.AbstractInJobOctoButton import AbstractInJobOctoButton


class PauseButton(AbstractInJobOctoButton):

    def _action_local(self) -> None:
        self._printer_api.get_job_facade().pause_toggle()
