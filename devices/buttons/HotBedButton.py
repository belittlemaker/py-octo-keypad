from devices.buttons.AbstractOutJobOctoButton import AbstractOutJobOctoButton
from devices.buttons.AbstractOptionsButton import AbstractOptionsButton


class HotBedButton(AbstractOutJobOctoButton, AbstractOptionsButton):

    def _action_local(self) -> None:
        temperature: int = self._get_option()

        command: str = 'M140 S{}'.format(temperature)

        self._printer_api.get_printer_facade().printer_command([command])

    def _get_options(self) -> list:
        # explore own bed temperatures
        return [
            60, # PLA > print
            0,  # cold
        ]
