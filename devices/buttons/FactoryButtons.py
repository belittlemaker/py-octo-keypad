from devices.buttons.CancelButton import CancelButton
from devices.buttons.HomeButton import HomeButton
from devices.buttons.HotBedButton import HotBedButton
from devices.buttons.HotToolButton import HotToolButton
from devices.buttons.IButton import IButton
from devices.buttons.LevelButton import LevelButton
from devices.buttons.PauseButton import PauseButton
from pyoctoprintapifacade import OctoPrintApi


class FactoryButtons:

    @staticmethod
    def get_cancel_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return CancelButton(pin, printer_api)

    @staticmethod
    def get_home_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return HomeButton(pin, printer_api)

    @staticmethod
    def get_hot_bed_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return HotBedButton(pin, printer_api)

    @staticmethod
    def get_hot_tool_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return HotToolButton(pin, printer_api)

    @staticmethod
    def get_level_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return LevelButton(pin, printer_api)

    @staticmethod
    def get_pause_button(pin: int, printer_api: OctoPrintApi) -> IButton:
        return PauseButton(pin, printer_api)
