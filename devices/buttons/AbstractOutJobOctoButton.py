from abc import ABC
from devices.buttons.AbstractOctoButton import AbstractOctoButton


class AbstractOutJobOctoButton(AbstractOctoButton, ABC):

    __allowed_states: list = ['Operational']

    def is_allowed_action(self) -> bool:
        allowed_action: bool = super().is_allowed_action()

        allowed_state: bool = self._environment.get_job_state() in self.__allowed_states

        return allowed_action and allowed_state
